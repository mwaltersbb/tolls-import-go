package main

import (
	"strconv"
	"log"
	"strings"
	"time"
	"fmt"
)

func dashToZeroLen(in *string) {
	if *in == "-" {
		*in = ""
	}
}

func ynToBool(in *string) bool {
	return *in == "Y"
}

func csvStrToInt(in string) int {
	out, err := strconv.Atoi(in)
	if err != nil {
		fmt.Println("Error in csvStrToInt")
		log.Fatal(err)
	}

	return out
}

func moneyStringToInt(in *string) (int, error) {
	temp := strings.Replace(*in, "$", "", -1)
	temp = strings.Replace(temp, ".", "", -1)
	temp = strings.Replace(temp, "(", "-", -1)
	temp = strings.Replace(temp, ")", "", -1)
	return strconv.Atoi(temp)
}

func bod(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, t.Location())
}

func getTime(in string) time.Time {
	const dateFormat = "2006-01-02 15:04:05"
	if in != "" {
		tm, err := time.Parse(dateFormat, in)
		if err != nil {
			log.Fatal(err)
		}
		return tm
	} else {
		return time.Time{}
	}

}

func inTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

func isZLenOrSpace(s string) bool {
	return s == "" || s == " "
}
