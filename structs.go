package main

import "time"

type Bestpass struct {
	postDate         time.Time
	transDesc        string
	tagNum           string
	truckNum         string
	plateNum         string
	plateState       string
	entryDT          time.Time
	entryPlaza       string
	entryPlazaName   string
	entryLane        string
	exitDT           time.Time
	exitPlaza        string
	exitPlazaName    string
	exitLane         string
	tollClass        string
	miles            float64
	amount           int
	agency           string
	tollType         string
	prepaid          bool
	service          string
	transId          string
	discounted       bool
	discountedAmount int
	costCenter       string
	unit             string
	transDT          time.Time
}

func (b Bestpass) IsZero() bool { return b.transDT.IsZero() }

type Actual struct {
	route         string
	driver        int
	driverName    string
	load          int
	contract      string
	segment       string
	loadRef       string
	stop          int
	locationName  string
	stopType      string
	plnArr        time.Time
	actArr        time.Time
	arrFlag       string
	plnDepart     time.Time
	actDepart     time.Time
	departFlag    string
	plnStopTime   int
	actStopTime   int
	stopFlag      string
	plnTravelTime int
	plnTravelDist int
	plnEquip      string
	actEquip      string
	plnTrailer    string
	stopState     string
	arrSource     string
	departSource  string
}

func (a Actual) IsZero() bool { return a.route == "" }

type Actuals []*Actual

func (slice Actuals) Len() int           { return len(slice) }
func (slice Actuals) Less(i, j int) bool { return slice[i].actDepart.Before(slice[j].actDepart) }
func (slice Actuals) Swap(i, j int)      { slice[i], slice[j] = slice[j], slice[i] }

type Employee struct {
	id             int
	employeeNumber int
}

func (e Employee) IsZero() bool { return e.id == 0 }

type Vehicle struct {
	id            int
	vehicleNumber string
}

func (v Vehicle) IsZero() bool { return v.id == 0 }

type Contract struct {
	id             int
	contractNumber string
}

func (c Contract) IsZero() bool { return c.id == 0 }

type Transponder struct {
	id           int
	serialNumber int
	vehicleId    int
	startDate    time.Time
	endDate      time.Time
}

func (t Transponder) IsZero() bool { return t.id == 0 }
