package main

import (
	"net/http"
	"log"
	"io/ioutil"
	"strings"
	"strconv"
	"regexp"
)

func getLoadtrekToken(ltUserPtr *string, ltPassPtr *string, ltCompPtr *string) string {
	reqUri := ltURI + "?action=authenticate"
	client := &http.Client{}
	req, err := http.NewRequest("GET", reqUri, nil)
	req.Header.Add("user", *ltUserPtr)
	req.Header.Add("password", *ltPassPtr)
	req.Header.Add("company", *ltCompPtr)

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return strings.Trim(string(bs), "\r\n")
}

func getLtActual(date int64, token string, ltCompPtr *string) string {
	reqURI := ltURI + "?action=run&dv=Actual+Info+By+Date&startDate=" + strconv.FormatInt(date, 10) + "&token=" + token + "&company=" + *ltCompPtr
	client := &http.Client{}
	req, _ := http.NewRequest("GET", reqURI, nil)
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	return string(bs)
}

func processActual(line []string) *Actual {
	record := Actual{
		route:         line[0],
		driver:        csvStrToInt(line[1]),
		driverName:    line[2],
		load:          csvStrToInt(line[3]),
		contract:      line[4],
		loadRef:       line[5],
		stop:          csvStrToInt(line[6]),
		locationName:  line[7],
		stopType:      line[8],
		plnArr:        getTime(line[9]),
		actArr:        getTime(line[10]),
		arrFlag:       line[11],
		plnDepart:     getTime(line[12]),
		actDepart:     getTime(line[13]),
		departFlag:    line[14],
		plnStopTime:   csvStrToInt(line[15]),
		actStopTime:   csvStrToInt(line[16]),
		stopFlag:      line[17],
		plnTravelTime: csvStrToInt(line[18]),
		plnTravelDist: csvStrToInt(line[19]),
		plnEquip:      line[20],
		actEquip:      line[21],
		plnTrailer:    line[22],
		stopState:     line[23],
		arrSource:     line[24],
		departSource:  line[25]}

	return &record
}

func getContractSegment(c string) (string, string) {
	var res1, res2 string
	re, _ := regexp.Compile("(.*)(\\(.\\))")
	resultSlice := re.FindAllStringSubmatch(c, -1)

	if len(resultSlice) > 0 {
		if len(resultSlice[0]) == 3 {
			res1 = resultSlice[0][1]
			res2 = resultSlice[0][2]
		} else if len(resultSlice[0]) == 2 {
			res1 = resultSlice[0][1]
			res2 = ""
		} else {
			res1 = ""
			res2 = ""
		}
	} else {
		res1 = ""
		res2 = ""
	}

	return res1, res2
}
