package main

import (
	"database/sql"
	"log"
	"time"
	_ "github.com/denisenkom/go-mssqldb"
)

func connectDb(user string, pass string) *sql.DB {
	connectionString := "server=sql2008r2vm;user_id=" + user + ";password=" + pass
	db, err := sql.Open("sqlserver", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	return db
}

func getEmployees(db *sql.DB) []Employee {
	query := `SELECT id, employeeNumber FROM erp.dbo.employees;`
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	var employees []Employee
	for rows.Next() {
		var id, employeeNumber int
		rows.Scan(&id, &employeeNumber)
		employees = append(employees, Employee{id, employeeNumber})
	}

	return employees
}

func getVehicles(db *sql.DB) []Vehicle {
	query := `SELECT id, vehicleNumber FROM erp.dbo.vehicles`
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	var vehicles []Vehicle
	for rows.Next() {
		var id int
		var vehicleNumber string
		rows.Scan(&id, &vehicleNumber)
		vehicles = append(vehicles, Vehicle{id, vehicleNumber})
	}

	return vehicles
}

func getContracts(db *sql.DB) []Contract {
	query := `SELECT id, contractNumber FROM erp.dbo.contracts`
	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	var contracts []Contract
	for rows.Next() {
		var id int
		var contractNumber string
		rows.Scan(&id, &contractNumber)
		contracts = append(contracts, Contract{id, contractNumber})
	}

	return contracts
}

func getTransponders(db *sql.DB) []Transponder {
	query := `SELECT
	transponders.id,
	transponders.SerialNumber,
	transponderAssignments.vehicleId,
	transponderAssignments.StartDate,
	ISNULL(transponderAssignments.endDate, '2099-12-31') endDate
FROM transponders
JOIN transponderAssignments
	ON transponderAssignments.TransponderId = transponders.id;`

	rows, err := db.Query(query)
	if err != nil {
		log.Fatal(err)
	}

	var transponders []Transponder
	for rows.Next() {
		var id, serialNumber, vehicleId int
		var startDate, endDate time.Time
		rows.Scan(&id, &serialNumber, &vehicleId, &startDate, &endDate)
		transponders = append(transponders, Transponder{id, serialNumber, vehicleId,
								startDate, endDate})
	}

	return transponders
}
