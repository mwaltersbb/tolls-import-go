package main

import (
	"io/ioutil"
	"time"
	"encoding/csv"
	"strings"
	"log"
	"strconv"
	"os"
	"flag"
	"sort"
	"regexp"
	"fmt"
)

const ltURI string = "https://web.loadtrek.net/dvservice"

type EpochRange [] int64

func (a EpochRange) Len() int           { return len(a) }
func (a EpochRange) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a EpochRange) Less(i, j int) bool { return a[i] < a[j] }

func main() {
	filePathPtr := flag.String("file", "", "Input file path")
	ltUserPtr := flag.String("ltuser", "", "Loadtrek username")
	ltPassPtr := flag.String("ltpass", "", "Loadtrek password")
	ltCompPtr := flag.String("ltcomp", "", "Loadtrek company")
	dbUserPtr := flag.String("dbuser", "", "DB Username")
	dbPassPtr := flag.String("dbpass", "", "DB Password")
	flag.Parse()

	if *filePathPtr == "" {
		panic("Give a path!")
	}

	if *ltUserPtr == "" || *ltPassPtr == "" || *ltCompPtr == "" || *dbUserPtr == "" || *dbPassPtr == "" {
		panic("You must provide all lt flags")
	}

	// Connect to the DB, open files
	db := connectDb(*dbUserPtr, *dbPassPtr)
	defer db.Close()
	f, err := os.Create("./out.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	dat, err := ioutil.ReadFile(*filePathPtr)
	if err != nil {
		panic(err)
	}

	contents := string(dat)
	parsed := parseCsv(&contents)

	var records []Bestpass
	var dates EpochRange
	for i := 1; i < len(parsed); i++ {
		record, date := processLine(parsed[i])
		records = append(records, record)
		dateBod := bod(date)
		thisEpoch := dateBod.UnixNano() / int64(time.Millisecond)
		var exists bool
		for _, epoch := range dates {
			if thisEpoch == epoch {
				exists = true
				break
			}
		}

		if !exists {
			dates = append(dates, thisEpoch)
		}
	}

	sort.Sort(dates)
	ltToken := getLoadtrekToken(ltUserPtr, ltPassPtr, ltCompPtr)

	var ltActualsCSV []string
	for _, date := range dates {
		ltActualsCSV = append(ltActualsCSV, getLtActual(date, ltToken, ltCompPtr))
	}

	var actuals Actuals
	for _, acts := range ltActualsCSV {
		acts = strings.Replace(acts, `"`, "", -1)
		rawAct := parseCsv(&acts)
		for i := 1; i < len(rawAct); i++ {
			actuals = append(actuals, processActual(rawAct[i]))
		}
	}

	employees := getEmployees(db)
	vehicles := getVehicles(db)
	contracts := getContracts(db)
	transponders := getTransponders(db)

	for _, record := range records {
		var thisContract Contract
		var thisActual Actual
		var thisSegment string

		thisTrans := findTrans(&record, &transponders)
		thisVehicle := findVehicle(&thisTrans, &vehicles)
		actsBefore := findActsBefore(&actuals, &thisVehicle, &record)

		if len(actsBefore) > 0 {
			sort.Sort(actsBefore)
			thisActual = *actsBefore[len(actsBefore)-1]
		}

		thisEmployee := findEmployee(&employees, &thisActual)

		if !thisActual.IsZero() {
			contract, segment := getContractSegment(thisActual.contract)
			thisSegment = segment
			for _, c := range contracts {
				if c.contractNumber == contract {
					thisContract = c
				}
			}
		}

		var ostr string
		var transIdStr string
		var cntIdStr string
		var vIdStr string
		var empIdStr string
		if !thisTrans.IsZero() {
			transIdStr = strconv.Itoa(thisTrans.id)
		}
		if !thisContract.IsZero() {
			cntIdStr = strconv.Itoa(thisContract.id)
		}
		if !thisVehicle.IsZero() {
			vIdStr = strconv.Itoa(thisVehicle.id)
		}
		if !thisEmployee.IsZero() {
			empIdStr = strconv.Itoa(thisEmployee.id)
		}

		ostr = ostr + record.postDate.Format("1/2/2006") + "," +
			record.transDT.Format("1/2/2006") + "," +
			transIdStr + "," +
			record.plateNum + "," +
			record.plateState + "," +
			cntIdStr + "," +
			vIdStr + "," +
			empIdStr + "," +
			thisSegment + "\r\n"

		f.WriteString(ostr)
	}
	os.Exit(0)
}

func parseCsv(csvStr *string) [][]string {
	r := csv.NewReader(strings.NewReader(*csvStr))

	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	return records
}

func processLine(parsed []string) (Bestpass, time.Time) {
	dtLayout := "01/02/2006 15:04:05"
	for n := 0; n < len(parsed); n++ {
		dashToZeroLen(&parsed[n])
	}

	postDate, err := time.Parse("01/02/2006", parsed[0])
	if err != nil {
		log.Fatal(err)
	}

	var entryDT time.Time
	if parsed[7] != "" && parsed[8] != "" {
		entryDT, err = time.Parse(dtLayout, parsed[7]+" "+parsed[8])
		if err != nil {
			log.Fatal(err)
		}
	} else {
		entryDT = time.Time{}
	}

	var exitDT time.Time
	if parsed[12] != "" && parsed[13] != "" {
		exitDT, err = time.Parse(dtLayout, parsed[12]+" "+parsed[13])
		if err != nil {
			log.Fatal(err)
		}
	} else {
		exitDT = time.Time{}
	}

	var miles float64
	if !isZLenOrSpace(parsed[18]) {
		miles, err = strconv.ParseFloat(parsed[18], 10)
		if err != nil {
			fmt.Println("Err parsing miles")
			log.Fatal(err)
		}
	}

	var amount int
	if !isZLenOrSpace(parsed[19]) {
		amount, err = moneyStringToInt(&parsed[19])
		if err != nil {
			fmt.Println("Err in parsing amount")
			log.Fatal(err)
		}
	}

	var prepaid bool
	if !isZLenOrSpace(parsed[24]) {
		prepaid = ynToBool(&parsed[24])
	}

	var discounted bool
	if !isZLenOrSpace(parsed[25]) {
		discounted = ynToBool(&parsed[25])
	}

	var discountedAmount int
	if !isZLenOrSpace(parsed[20]) {
		discountedAmount, err = moneyStringToInt(&parsed[20])
		if err != nil {
			fmt.Println("Err in discounted amount")
			log.Fatal(err)
		}
	}

	record := Bestpass{
		postDate:         postDate,
		transDesc:        parsed[1],
		tagNum:           parsed[2],
		truckNum:         parsed[5],
		plateNum:         parsed[3],
		plateState:       parsed[4],
		entryPlaza:       parsed[9],
		entryPlazaName:   parsed[10],
		entryLane:        parsed[11],
		exitPlaza:        parsed[14],
		exitPlazaName:    parsed[15],
		exitLane:         parsed[16],
		tollClass:        parsed[17],
		miles:            miles,
		amount:           amount,
		agency:           parsed[22],
		tollType:         parsed[26],
		prepaid:          prepaid,
		service:          parsed[23],
		transId:          parsed[27],
		discounted:       discounted,
		discountedAmount: discountedAmount,
		costCenter:       parsed[6],
		unit:             parsed[5]}

	if !entryDT.IsZero() {
		record.entryDT = entryDT
		record.transDT = time.Time(entryDT)
	}

	if !exitDT.IsZero() {
		record.exitDT = exitDT
		if record.entryDT.IsZero() {
			record.transDT = time.Time(exitDT)
		}
	}

	return record, record.transDT
}

func findTrans(record *Bestpass, trans *[]Transponder) Transponder {
	var t Transponder
	for _, tran := range *trans {
		res, _ := regexp.MatchString("-", record.tagNum)

		if !res {
			uintTagNum, err := strconv.ParseUint(record.tagNum, 10, 64)
			uintSerialNum := uint64(tran.serialNumber)
			if err != nil {
				log.Fatal(err)
			}
			if uintSerialNum == uintTagNum &&
				inTimeSpan(tran.startDate, tran.endDate, record.transDT) {
				t = tran
			}
		}
	}
	return t
}

func findVehicle(trans *Transponder, vehicles *[]Vehicle) Vehicle {
	var v Vehicle
	if !trans.IsZero() {
		for _, vehicle := range *vehicles {
			if vehicle.id == trans.vehicleId {
				v = vehicle
			}
		}
	}
	return v
}

func findActsBefore(acts *Actuals, v *Vehicle, r *Bestpass) Actuals {
	var actsBefore Actuals
	if !v.IsZero() {
		for _, act := range *acts {
			if act.actEquip == v.vehicleNumber && act.actDepart.Before(r.transDT) {
				actsBefore = append(actsBefore, act)
			}
		}
	}
	return actsBefore
}

func findEmployee(employees *[]Employee, act *Actual) Employee {
	var e Employee
	if !act.IsZero() {
		for _, emp := range *employees {
			if emp.employeeNumber == act.driver {
				e = emp
			}
		}
	}
	return e
}
